from entities.models import Person, Pet


def person_constructor_example():
    fluffy = Pet(name="Fluffy", age="3")
    alice = Person(first_name="Alice", last_name="Springs", pet=[fluffy])
    print(alice)


def person_from_data_example():
    person_data = {
        "first_name": "Joe",
        "last_name": "Bloggs",
        "age": 21,
        "pets": [{"name": "Rover", "age": 5}],
    }

    joe = Person(**person_data)

    print(joe)


def main():
    person_constructor_example()
    person_from_data_example()


if __name__ == "__main__":
    main()
