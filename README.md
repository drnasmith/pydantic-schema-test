# pydantic-schema-test

Example use of json schema and pydantic for model generation

## Setup and run
- Run scripts/codegen to generate entities/models.py
- Run pytest to check model import and validation
- Run python main.py to create basic Person and Pet models

## Explanation
- Code demonstrates use of json schema file to describe Person,Pet models
- Script generates pydantic models using the datamodel-code-generator app
- Test code and app use the pydantic models to store Person/Pet relationships
