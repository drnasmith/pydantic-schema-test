@ECHO OFF
@ECHO Generating models from entities/schema.json
@ECHO Output to entities/models.py
datamodel-codegen  --input ../entities/schema.json --input-file-type jsonschema --output ../entities/models.py