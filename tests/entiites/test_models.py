import pytest
from pydantic import ValidationError
from entities.models import Person, Pet


def test_person_model_valid():
    try:
        person = Person(first_name="Alice", last_name="Springs")
    except ValidationError:
        assert False


def test_person_model_valid_from_data():
    data = {
        "first_name": "Joe",
        "last_name": "Bloggs",
        "age": 21,
        "pets": [{"name": "Rover", "age": 5}],
    }
    try:
        person = Person(**data)
    except ValidationError:
        assert False


def test_person_model_invalid_from_data():
    with pytest.raises(ValidationError):
        data = {}
        person = Person(**data)


def test_person_model_invalid():
    with pytest.raises(ValidationError):
        person = Person()


def test_pet_model_valid():
    try:
        pet1 = Pet()
        pet2 = Pet(name="Fluffy", age="2")
    except ValidationError:
        assert False


def test_pet_model_invalid():
    with pytest.raises(ValidationError):
        pet = Pet(name=[])
